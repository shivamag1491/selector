import React, { Component } from 'react';
import './App.scss';
import { Icon, Table, Header } from 'semantic-ui-react'

class App extends Component {
constructor(props){
    super(props);
    this.state = {
      roomCount: 1,
      adultCount: 1,
      childCount: 0,
      roomIncrease: false,
      roomDecrease: true,
      adultIncrease: false,
      adultDecrease: true,
      childIncrease: false,
      childDecrease: true
    }
    
    setInterval(()=>{
      if(1 < this.state.roomCount && this.state.roomCount < 5){
        this.setState({
          ...this.state,
          roomDecrease: false,
          roomIncrease: false
        });
      }else if(this.state.roomCount === 1){
        this.setState({
          ...this.state,
          roomDecrease: true,
          roomIncrease: false
        });
      }else if(this.state.roomCount === 5){
        this.setState({
          ...this.state,
          roomDecrease: false,
          roomIncrease: true
        });
      }
    
      // if(this.state.adultCount < 20 && this.state.childCount >= 0 &&
      //   (20 > (this.state.childCount + this.state.adultCount)) && (this.state.roomCount <= 5)){
      //     this.setState({
      //       ...this.state,
      //       adultIncrease: false,
      //       adultDecrease: false
      //     });
      //   }else
        if(this.state.adultCount === 1){
          this.setState({
            ...this.state,
            adultIncrease: false,
            adultDecrease: true
          });
        }else if(this.state.adultCount === 20){
          this.setState({
            ...this.state,
            adultIncrease: true,
            adultDecrease: false
          });
        }else if(20 === (this.state.childCount + this.state.adultCount)){
          this.setState({
            ...this.state,
            adultIncrease: true,
            adultDecrease: false
          });
        }
  
        // if(this.state.childCount < 15 && (this.state.adultCount + this.state.childCount) < 20){
        //   this.setState({
        //     ...this.state,
        //     childIncrease: false,
        //     childDecrease: false
        //   });
        // }
    }, 300);
  }
    
  /**
   * @Desc Func to handle count increase when user click on increase button
   */
  handleIncrement = (tag) => {
    switch (tag) {
      case 'room':
        if( 1 <= this.state.roomCount && this.state.roomCount < 5){
          this.setState({
            ...this.state,
            roomIncrease: false,
            roomDecrease: false,
            roomCount: this.state.roomCount + 1
          });
        }else{
          this.setState({
            ...this.state,
            roomIncrease: true,
            roomDecrease: false
          });
        }
        setTimeout(()=>{
          if(this.state.adultCount < this.state.roomCount && this.state.roomCount < 6){
            this.setState({
              adultCount: this.state.adultCount + 1
            });
          }
        }, 300);
      break;
      case 'adult':
        if(this.state.adultCount < 20 && this.state.childCount >= 0 &&
              (20 > (this.state.childCount + this.state.adultCount)) && (this.state.roomCount <= 5)){
          this.setState({
            ...this.state,
            adultIncrease: false,
            adultDecrease: false,           
            adultCount: this.state.adultCount + 1
          });
       }else{
          this.setState({
            ...this.state,
            adultIncrease: true,
            adultDecrease: false
          });
        }
        setTimeout(()=>{
          if((this.state.adultCount + this.state.childCount) > (this.state.roomCount * 4)){
            this.setState({
              ...this.state,
              roomCount: this.state.roomCount + 1
            });
          }
          if(this.state.childCount !== 0 && ((this.state.roomCount * 4) < (this.state.childCount + this.state.adultCount))){
            this.setState({
              childCount: this.state.childCount - 1
            });
          }
        }, 300);
      break;
     case 'child':
        if(this.state.childCount < 15 && (this.state.adultCount + this.state.childCount) < 20){
          this.setState({
            ...this.state,
            childIncrease: false,
            childDecrease: false,
            childCount: this.state.childCount + 1
          });
        }else{
     this.setState({
       ...this.state,
       childIncrease: true,
       childDecrease: false
     });
   }
   setTimeout(()=>{
     if((this.state.adultCount + this.state.childCount) > (this.state.roomCount * 4)){
       this.setState({
         ...this.state,
         roomCount: this.state.roomCount + 1
       });
       if(this.state.roomCount > this.state.adultCount){
         this.setState({
           ...this.state,
           adultCount: this.state.adultCount + 1
         });
       }
     }
   }, 300);
 break;
 default:
   break;
   }
  }

  /**
   * @Desc Func to handle count decrease when user click on decrease button
   */
  handleDecrement = (tag) => {
    switch (tag) {
 case 'room':
   if(this.state.roomCount > 1){
     this.setState({
       ...this.state,
       roomIncrease: false,
       roomDecrease: false,
       roomCount: this.state.roomCount - 1
     });
   }else{
     this.setState({
       ...this.state,
       roomIncrease: false,
       roomDecrease: true,
     });
   }
   setTimeout(()=>{
     if((this.state.adultCount + this.state.childCount) > (this.state.roomCount * 4)){
       this.setState({
         childCount: 0
       });
     }
     if(this.state.adultCount > (this.state.roomCount*4)){
       this.setState({
         adultCount: this.state.roomCount * 4
       });
     }
   }, 200);
 break;
 case 'adult':
   if(this.state.adultCount > 1 ){
     this.setState({
       ...this.state,
       adultIncrease: false,
            adultDecrease: false,
            adultCount: this.state.adultCount - 1
          });
        }else{
          this.setState({
            ...this.state,
            adultIncrease: false,
            adultDecrease: true,
          });
        }

         setTimeout(()=>{
        if(this.state.adultCount >= 1 && (this.state.adultCount < this.state.roomCount)){
            this.setState({
              ...this.state,
              roomCount: this.state.roomCount - 1,
            });
          }
          if((this.state.adultCount + this.state.childCount) === ((this.state.roomCount-1) * 4)){
            this.setState({
              ...this.state,
              roomCount: this.state.roomCount - 1
            });
          }
          if((this.state.adultCount + this.state.childCount) > ((this.state.roomCount) * 4)){
            this.setState({
              ...this.state,
              childCount: (this.state.roomCount * 4 - this.state.adultCount)
            });
          }
        }, 300);
      break;
      case 'child':
        if(this.state.childCount > 0){
          this.setState({
            ...this.state,
            childIncrease: false,
            childDecrease: false,
            childCount: this.state.childCount - 1
          });
        }else{
          this.setState({
            ...this.state,
            childIncrease: false,
            childDecrease: true
          });
        }
        setTimeout(()=>{
          if((this.state.adultCount + this.state.childCount) === ((this.state.roomCount-1) * 4)){
            this.setState({
              ...this.state,
              roomCount: this.state.roomCount - 1
            });
          }
        }, 300);
      break;
      default:
        break;
    }
  }
    
       render() {
         return (
           <div className="App">
      <Header className='app-header'>Room Selector</Header>
 <span className='header'><Icon name='users' alt='people-icon' /> Choose number of <span className='text'>people</span></span>
 <Table>
   <Table.Body>
     <Table.Row>
       <Table.Cell>
         <Icon name='bed' className='header-icon' />
           <span className='header-text'>Rooms</span>
       </Table.Cell>
       <Table.Cell>
         <div className='actions'>
           <Icon name='plus circle' onClick={() => this.handleIncrement('room')} disabled={this.state.roomIncrease}/>
             <span>{this.state.roomCount}</span>
           <Icon name='minus circle' onClick={() => this.handleDecrement('room')} disabled={this.state.roomDecrease}/>
         </div>
       </Table.Cell>
     </Table.Row>
     <Table.Row>
       <Table.Cell>
       <Icon name='user' className='header-icon'/>
         <span className='header-text'>Adults</span>
       </Table.Cell>
       <Table.Cell>
           <div className='actions'>
           <Icon name='plus circle' onClick={() => this.handleIncrement('adult')} disabled={this.state.adultIncrease}/>
             <span>{this.state.adultCount}</span>
           <Icon name='minus circle' onClick={() => this.handleDecrement('adult')} disabled={this.state.adultDecrease}/>
           </div>
       </Table.Cell>
     </Table.Row>
     <Table.Row>
       <Table.Cell>
         <Icon name='child' className='header-icon'/>
           <span className='header-text'>Child</span>
         </Table.Cell>
       <Table.Cell>
       <div className='actions'>
         <Icon name='plus circle' onClick={() => this.handleIncrement('child')} disabled={this.state.childIncrease}/>
           <span>{this.state.childCount}</span>
         <Icon name='minus circle' onClick={() => this.handleDecrement('child')} disabled={this.state.childDecrease}/>
       </div>
</Table.Cell>
     </Table.Row>
   </Table.Body>
 </Table>
       </div>
     );
   }
}

export default App;
