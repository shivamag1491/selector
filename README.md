## Room Selector Implementation
    This is meant for user to able to choose room in a hotel & can choose number of person ( adult & child ) with the available constraints.
    For refer follow room-selector.pdf file.

### Compatibility

#### Browsers
  * Chrome
  * Firebox
  * Safari
  * Internet Explorer

### Development

#### Dependencies
  * Node Version 8.11.2
  * Npm Version 6.1.0

#### Installation

* git clone https://shivamag1491@bitbucket.org/shivamag1491/selector.git

* cd selector
* Use command 'npm install' to install the dependencies.
* Run command 'npm start' to start the dev server locally at http://localhost:3000

#### Development
* 'npm run-script build' builds the app and generates a build folder at root directory
